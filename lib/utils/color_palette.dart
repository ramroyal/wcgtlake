import 'package:flutter/cupertino.dart';

class AppColors {
  static const Color primaryColor = Color(0xFF4F2D7F);
  static const Color accentColor = Color(0xFFFCB050);
  static const Color black = Color(0xFF001517);
  static const Color dark_grey = Color(0xFF999999);
  static const Color light_grey = Color(0xFFB2B8B9);
  static const Color white = Color(0xFFFFFFFF);
  static const Color light_blue = Color(0xFF30C4E5);
}
