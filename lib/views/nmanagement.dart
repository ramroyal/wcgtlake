import 'package:flutter/material.dart';
import 'package:wcgtlake/utils/color_palette.dart';

class NManagementView extends StatelessWidget {
  final String appBarTitle;
  final List<User> _userList = [
    User('Vishvesh C Chandiok', 'CEO'),
    User('Amit Jaiswal', 'COO'),
    User('Siddhartha Nigam', 'NMP - C&M & Growth'),
    User('Sumesh EdakkalathilS', 'NMP - P&C'),
  ];

  NManagementView({Key key, this.appBarTitle}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_rounded,
          ),
          onPressed: () => Navigator.pop(context),
        ),
        title: Text(
          appBarTitle,
        ),
      ),
      body: Container(
        padding: const EdgeInsets.all(10.0),
        child: ListView.builder(
          itemCount: _userList.length,
          itemBuilder: (context, index) {
            var listItem = _userList[index];
            return Container(
              margin:
                  const EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
              decoration: BoxDecoration(
                  color: AppColors.white,
                  borderRadius: BorderRadius.circular(15.0),
                  boxShadow: [
                    BoxShadow(
                      color: AppColors.primaryColor.withOpacity(0.1),
                      blurRadius: 10.0,
                      spreadRadius: 1.0,
                    )
                  ]),
              child: ListTile(
                leading: CircleAvatar(
                  backgroundImage: AssetImage('assets/images/profile.jpg'),
                  radius: 24.0,
                ),
                title: Text(
                  listItem.name,
                  style: Theme.of(context).textTheme.bodyText1.copyWith(
                      fontSize: 15.0,
                      color: AppColors.primaryColor,
                      fontWeight: FontWeight.bold),
                ),
                subtitle: Text(
                  listItem.designation,
                  style: Theme.of(context).textTheme.bodyText1.copyWith(
                      fontSize: 15.0,
                      color: AppColors.dark_grey,
                      fontWeight: FontWeight.bold),
                ),
                trailing: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    IconButton(
                      constraints: BoxConstraints(),
                      onPressed: () {},
                      icon: Image.asset(
                        'assets/images/twitter.png',
                      ),
                    ),
                    IconButton(
                      constraints: BoxConstraints(),
                      onPressed: () {},
                      icon: Image.asset(
                        'assets/images/linkedin.png',
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}

class User {
  String name;
  String designation;

  User(this.name, this.designation);
}
