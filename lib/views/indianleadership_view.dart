import 'package:flutter/material.dart';
import 'package:wcgtlake/utils/color_palette.dart';
import 'package:wcgtlake/views/nmanagement.dart';

class IndianLeadershipView extends StatelessWidget {
  final String appBarTitle;

  const IndianLeadershipView({Key key, this.appBarTitle}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<String> _listTitle = [
      'National Management',
      'Expertise National Manager Partner(NMPs)',
      'Market Territory Business Unit Leader(MTBULs)',
      'Strategic BUs',
      'Incubation BUs',
      'Regional Expertise Leader (RELs)'
          'Sector Leaders',
      'Channel Leaders',
      'Segment Leaders',
      'Corridor Leaders',
      'Operations Leader'
    ];
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_rounded,
          ),
          onPressed: () => Navigator.pop(context),
        ),
        title: Text(
          appBarTitle,
        ),
      ),
      body: ListView.separated(
        itemBuilder: (context, index) => ListTile(
          title: Text(
            _listTitle[index],
            style: Theme.of(context).textTheme.bodyText2,
          ),
          trailing: Icon(
            Icons.chevron_right_outlined,
          ),
          onTap: () {
            if (index == 0) {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => NManagementView(
                    appBarTitle: 'National Management',
                  ),
                ),
              );
            }
          },
        ),
        separatorBuilder: (context, index) => Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Divider(
            color: AppColors.light_grey,
            height: 1,
          ),
        ),
        itemCount: _listTitle.length,
      ),
    );
  }
}
