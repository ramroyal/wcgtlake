import 'package:flutter/material.dart';
import 'package:wcgtlake/utils/color_palette.dart';
import 'package:wcgtlake/views/indianleadership_view.dart';
import '../utils/color_palette.dart';

class DrawerView extends StatelessWidget {
  const DrawerView({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(5.0, 40.0, 20.0, 20.0),
        child: Column(
          children: [
            Row(
              children: [
                IconButton(
                  icon: Icon(Icons.arrow_back_ios),
                  onPressed: () => Navigator.pop(context),
                ),
                CircleAvatar(
                  backgroundImage: AssetImage('assets/images/profile.jpg'),
                  radius: 20.0,
                ),
                const SizedBox(
                  width: 10.0,
                ),
                Text(
                  'Danny Bulheikn',
                  style: Theme.of(context).textTheme.headline6.copyWith(
                      color: AppColors.primaryColor,
                      fontWeight: FontWeight.bold),
                )
              ],
            ),
            SizedBox(height: 20.0),
            Theme(
              data: ThemeData().copyWith(
                dividerColor: Colors.transparent,
                accentColor: AppColors.primaryColor,
                unselectedWidgetColor: AppColors.primaryColor,
              ),
              child: Expanded(
                child: ListView(
                  padding: EdgeInsets.zero,
                  children: [
                    ExpansionTile(
                      title: Text(
                        'Our Firm',
                        style: Theme.of(context)
                            .textTheme
                            .headline6
                            .apply(color: AppColors.primaryColor),
                      ),
                      children: <Widget>[
                        ListTile(
                          title: Text(
                            'Ceo Corner',
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1
                                .apply(color: AppColors.primaryColor),
                          ),
                        ),
                        ListTile(
                          onTap: () {
                            Navigator.pop(context);
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) => IndianLeadershipView(
                                  appBarTitle: 'India Leadership',
                                ),
                              ),
                            );
                          },
                          title: Text(
                            'India Leadership',
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1
                                .apply(color: AppColors.primaryColor),
                          ),
                        ),
                        ListTile(
                          title: Text(
                            'Partnership Board',
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1
                                .apply(color: AppColors.primaryColor),
                          ),
                        ),
                        ListTile(
                          title: Text(
                            'Leadership Communications',
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1
                                .apply(color: AppColors.primaryColor),
                          ),
                        ),
                        ListTile(
                          title: Text(
                            'Newly Promoted',
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1
                                .apply(color: AppColors.primaryColor),
                          ),
                        ),
                        ListTile(
                          title: Text(
                            'IPC 2021',
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1
                                .apply(color: AppColors.primaryColor),
                          ),
                        ),
                        ListTile(
                          title: Text(
                            'IPC 2020',
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1
                                .apply(color: AppColors.primaryColor),
                          ),
                        ),
                        ListTile(
                          title: Text(
                            'Quaterly IPC NOV 2020',
                            style: Theme.of(context)
                                .textTheme
                                .bodyText1
                                .apply(color: AppColors.primaryColor),
                          ),
                        ),
                      ],
                    ),
                    ListTile(
                      title: Text(
                        'Supporting You',
                        style: Theme.of(context).textTheme.headline6,
                      ),
                    ),
                    ListTile(
                      title: Text(
                        'Sector',
                        style: Theme.of(context).textTheme.headline6,
                      ),
                    ),
                    ListTile(
                      title: Text(
                        'EBU Councils',
                        style: Theme.of(context).textTheme.headline6,
                      ),
                    ),
                    ListTile(
                      title: Text(
                        'The proposal studio',
                        style: Theme.of(context).textTheme.headline6,
                      ),
                    ),
                    ListTile(
                      title: Text(
                        'Covid 19',
                        style: Theme.of(context).textTheme.headline6,
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
