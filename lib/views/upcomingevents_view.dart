import 'package:flutter/material.dart';
import 'package:wcgtlake/utils/color_palette.dart';

class UpComingEventsView extends StatelessWidget {
  final appBarTitle;

  const UpComingEventsView({
    Key key,
    this.appBarTitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text('Upcoming Events'),
        actions: [
          IconButton(
            icon: Icon(Icons.close),
            onPressed: () => Navigator.pop(context),
          )
        ],
      ),
      body: Container(
        width: double.infinity,
        height: 285,
        child: ListView.builder(
          physics: NeverScrollableScrollPhysics(),
          itemCount: 2,
          itemBuilder: (context, index) => Container(
            padding: const EdgeInsets.all(12.0),
            margin: const EdgeInsets.all(5.0),
            decoration: BoxDecoration(
              color: AppColors.accentColor,
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: Container(
              height: 110,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    width: 90,
                    decoration: BoxDecoration(
                      color: AppColors.white,
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          '27',
                          style: Theme.of(context)
                              .textTheme
                              .headline5
                              .copyWith(color: AppColors.accentColor),
                        ),
                        Text(
                          'APR',
                          style: Theme.of(context).textTheme.bodyText2.copyWith(
                              color: AppColors.black,
                              fontWeight: FontWeight.bold),
                        ),
                        Text(
                          '2021',
                          style: Theme.of(context).textTheme.bodyText1.copyWith(
                              color: AppColors.dark_grey,
                              fontWeight: FontWeight.w200),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Sustainable Automotive Technologies - The road ahead for e-mobility',
                            style: Theme.of(context)
                                .textTheme
                                .bodyText2
                                .copyWith(color: AppColors.white),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            children: [
                              Icon(
                                Icons.location_on_outlined,
                                color: AppColors.white,
                                size: 16.0,
                              ),
                              Text(
                                'Virtual Event',
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText2
                                    .copyWith(color: AppColors.white),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
