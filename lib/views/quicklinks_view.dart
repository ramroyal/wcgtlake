import 'package:flutter/material.dart';
import 'package:wcgtlake/utils/color_palette.dart';

class QuickLinksView extends StatelessWidget {
  final List<String> _quickLinksList = [
    'Employee Service Desk',
    'Global Independence System',
    'Global Restricted List',
    'Book My Desk',
    'Compliance Library',
    'Help Desk',
    'Pace',
    'Referral Portal',
    'SuperCoach Scorecards'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text('Quick Links'),
        actions: [
          IconButton(
            icon: Icon(Icons.close),
            onPressed: () => Navigator.pop(context),
          )
        ],
      ),
      body: Container(
        child: ListView.builder(
          physics: AlwaysScrollableScrollPhysics(),
          itemCount: _quickLinksList.length,
          itemBuilder: (context, index) {
            var listItem = _quickLinksList[index];
            return Container(
              margin:
                  const EdgeInsets.symmetric(vertical: 5.0, horizontal: 36.0),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12.0),
                  border: Border.all(
                    color: AppColors.light_blue,
                    width: 1.5,
                  )),
              padding: const EdgeInsets.all(16.0),
              alignment: Alignment.center,
              child: Text(
                listItem,
                style: Theme.of(context)
                    .textTheme
                    .bodyText2
                    .copyWith(color: AppColors.primaryColor),
              ),
            );
          },
        ),
      ),
    );
  }
}
