import 'package:flutter/material.dart';
import 'package:wcgtlake/utils/color_palette.dart';
import 'package:wcgtlake/views/eventdetails_view.dart';
import 'package:wcgtlake/views/joinees_view.dart';
import 'package:wcgtlake/views/quicklinks_view.dart';
import 'package:wcgtlake/views/toolsdatabase_view.dart';
import 'package:wcgtlake/views/upcomingevents_view.dart';

class HomeView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _buildHeader(context),
                _buildBanner(),
                _buildFirmSpotlight(context),
                _buildQuickLinks(context),
                _buildTipOfDay(context),
                _buildUpcomingEvents(context),
                _buildToolsAndDatabase(context),
                _buildFooter(context),
                _buildSocialPlatforms(context)
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildHeader(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 30.0),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Image.asset(
                'assets/images/lakelogo.png',
                height: 32.0,
              ),
              Padding(
                padding: const EdgeInsets.only(right: 10.0),
                child: Image.asset(
                  'assets/images/thorntonlogo.png',
                  height: 40.0,
                  width: 40.0,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Column _buildBanner() {
    return Column(
      children: [
        SizedBox(
          height: 20.0,
        ),
        Container(
          width: double.infinity,
          height: 180,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: 2,
            itemBuilder: (context, index) => Padding(
              padding: const EdgeInsets.only(right: 10.0),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(16.0),
                child: Image.asset(
                  'assets/images/LifeatGT_banner.png',
                ),
              ),
            ),
          ),
        ),
        SizedBox(
          height: 20,
        ),
      ],
    );
  }

  Widget _buildFirmSpotlight(BuildContext context) {
    return Column(
      children: [
        Column(
          children: [
            _buildSectionHeader(context, 'Firm in the spotlight', () {}),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        Container(
          width: double.infinity,
          height: 300,
          child: ListView.separated(
            separatorBuilder: (context, index) => Divider(
              height: 1,
              color: AppColors.light_grey,
            ),
            physics: NeverScrollableScrollPhysics(),
            itemCount: 3,
            itemBuilder: (context, index) => Container(
              padding:
                  const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
              margin: const EdgeInsets.symmetric(vertical: 5.0),
              decoration: BoxDecoration(
                color: AppColors.white,
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: Container(
                height: 80.0,
                child: Row(
                  children: [
                    Container(
                      width: 80,
                      decoration: BoxDecoration(
                          color: AppColors.white,
                          borderRadius: BorderRadius.circular(15.0),
                          boxShadow: [
                            BoxShadow(
                              color: AppColors.primaryColor.withOpacity(0.1),
                              blurRadius: 10.0,
                              spreadRadius: 1.0,
                            )
                          ]),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            '27',
                            style: Theme.of(context).textTheme.headline5,
                          ),
                          Text(
                            'Jan',
                            style: Theme.of(context)
                                .textTheme
                                .headline6
                                .copyWith(fontWeight: FontWeight.w200),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Transparency, good governance never fail on enterprenuer S Viswanatha Prasad of Caspian Debth',
                            style: Theme.of(context).textTheme.bodyText1,
                            maxLines: 4,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
        SizedBox(
          height: 20,
        ),
      ],
    );
  }

  Widget _buildQuickLinks(BuildContext context) {
    return Column(
      children: [
        Column(
          children: [
            _buildSectionHeader(
              context,
              'Quick Links',
              () => Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => QuickLinksView(),
                ),
              ),
            ),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        Container(
          width: double.infinity,
          height: 60,
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: 3,
            itemBuilder: (context, index) => Container(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              margin: const EdgeInsets.only(right: 10.0),
              decoration: BoxDecoration(
                color: AppColors.white,
                borderRadius: BorderRadius.circular(15),
                border: Border.all(color: AppColors.light_blue, width: 2.0),
              ),
              child: Center(
                child: Text(
                  'WCGT360*',
                  style: Theme.of(context)
                      .textTheme
                      .bodyText2
                      .copyWith(color: AppColors.primaryColor),
                ),
              ),
            ),
          ),
        ),
        SizedBox(
          height: 30,
        ),
      ],
    );
  }

  Widget _buildUpcomingEvents(BuildContext context) {
    return Column(
      children: [
        Column(
          children: [
            _buildSectionHeader(
              context,
              'Upcoming Events',
              () => Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => UpComingEventsView(),
                ),
              ),
            ),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        Container(
          width: double.infinity,
          height: 285,
          child: ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            itemCount: 2,
            itemBuilder: (context, index) => GestureDetector(
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => EventDetailsView(
                      appBarTitle: 'UpComing Events',
                    ),
                  ),
                );
              },
              child: Container(
                padding: const EdgeInsets.all(12.0),
                margin: const EdgeInsets.all(5.0),
                decoration: BoxDecoration(
                  color: AppColors.accentColor,
                  borderRadius: BorderRadius.circular(20.0),
                ),
                child: Container(
                  height: 110,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: 90,
                        decoration: BoxDecoration(
                          color: AppColors.white,
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              '27',
                              style: Theme.of(context)
                                  .textTheme
                                  .headline5
                                  .copyWith(color: AppColors.accentColor),
                            ),
                            Text(
                              'APR',
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText2
                                  .copyWith(
                                      color: AppColors.black,
                                      fontWeight: FontWeight.bold),
                            ),
                            Text(
                              '2021',
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyText1
                                  .copyWith(
                                      color: AppColors.dark_grey,
                                      fontWeight: FontWeight.w200),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                'Sustainable Automotive Technologies - The road ahead for e-mobility',
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText2
                                    .copyWith(color: AppColors.white),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Row(
                                children: [
                                  Icon(
                                    Icons.location_on_outlined,
                                    color: AppColors.white,
                                    size: 16.0,
                                  ),
                                  Text(
                                    'Virtual Event',
                                    style: Theme.of(context)
                                        .textTheme
                                        .bodyText2
                                        .copyWith(color: AppColors.white),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
        SizedBox(
          height: 20,
        ),
      ],
    );
  }

  Widget _buildSectionHeader(
      BuildContext context, String headerTitle, Function onClick) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          headerTitle,
          style: Theme.of(context).textTheme.headline5,
        ),
        GestureDetector(
          onTap: onClick,
          child: Text(
            'View all',
            style: Theme.of(context)
                .textTheme
                .bodyText2
                .copyWith(fontWeight: FontWeight.w200),
          ),
        )
      ],
    );
  }

  Widget _buildToolsAndDatabase(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Tools & Databases',
          style: Theme.of(context).textTheme.headline5,
        ),
        SizedBox(height: 10.0),
        GestureDetector(
          onTap: () => Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => ToolsDatabaseView(
                appBarTitle: 'Tools and Database',
              ),
            ),
          ),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(20.0),
            child: Image.asset(
              'assets/images/toolsand-database.jpeg',
              fit: BoxFit.cover,
            ),
          ),
        ),
        SizedBox(
          height: 20.0,
        )
      ],
    );
  }

  Widget _buildTipOfDay(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Wellness Tips for the Day',
          style: Theme.of(context).textTheme.headline5,
        ),
        SizedBox(
          height: 10,
        ),
        Container(
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                blurRadius: 10.0,
                spreadRadius: 1.0,
                color: AppColors.primaryColor.withOpacity(0.1),
              ),
            ],
          ),
          width: double.infinity,
          child: Image.asset(
            'assets/images/wellnessbanner.png',
            fit: BoxFit.cover,
          ),
        ),
        SizedBox(
          height: 20,
        ),
      ],
    );
  }

  Widget _buildFooter(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          onTap: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (context) => JoineesView(
                  appBarTitle: 'New Joinees',
                ),
              ),
            );
          },
          child: ClipRRect(
            borderRadius: BorderRadius.circular(20.0),
            child: Image.asset(
              'assets/images/welcome-firm.jpeg',
            ),
          ),
        ),
        SizedBox(
          height: 20,
        ),
        ClipRRect(
          borderRadius: BorderRadius.circular(20.0),
          child: Image.asset('assets/images/New-Joiners-Hub.jpeg'),
        ),
        SizedBox(
          height: 30.0,
        )
      ],
    );
  }

  Widget _buildSocialPlatforms(BuildContext context) {
    return Column(
      children: [
        _buildSectionHeader(context, 'Social Plaftforms', () {}),
        SizedBox(
          height: 20,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(
              height: 60,
              width: 60,
              padding: const EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                  color: AppColors.white,
                  borderRadius: BorderRadius.circular(15.0),
                  boxShadow: [
                    BoxShadow(
                      color: AppColors.primaryColor.withOpacity(0.1),
                      blurRadius: 10.0,
                      spreadRadius: 1.0,
                    )
                  ]),
              child: Image.asset(
                'assets/images/twitter.png',
              ),
            ),
            Container(
              height: 60,
              width: 60,
              padding: const EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                  color: AppColors.white,
                  borderRadius: BorderRadius.circular(15.0),
                  boxShadow: [
                    BoxShadow(
                      color: AppColors.primaryColor.withOpacity(0.1),
                      blurRadius: 10.0,
                      spreadRadius: 1.0,
                    )
                  ]),
              child: Image.asset(
                'assets/images/yammer.png',
              ),
            ),
            Container(
              height: 60,
              width: 60,
              padding: const EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                  color: AppColors.white,
                  borderRadius: BorderRadius.circular(15.0),
                  boxShadow: [
                    BoxShadow(
                      color: AppColors.primaryColor.withOpacity(0.1),
                      blurRadius: 10.0,
                      spreadRadius: 1.0,
                    )
                  ]),
              child: Image.asset(
                'assets/images/linkedin.png',
              ),
            )
          ],
        ),
        SizedBox(
          height: 40,
        ),
      ],
    );
  }
}
