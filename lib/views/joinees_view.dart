import 'package:flutter/material.dart';
import 'package:wcgtlake/utils/color_palette.dart';

class JoineesView extends StatelessWidget {
  final String appBarTitle;

  const JoineesView({
    Key key,
    this.appBarTitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final headingStyle = Theme.of(context)
        .textTheme
        .bodyText2
        .apply(color: AppColors.primaryColor, fontWeightDelta: 1);
    final bodyStyle = Theme.of(context)
        .textTheme
        .bodyText1
        .apply(color: AppColors.primaryColor);
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_rounded,
          ),
          onPressed: () => Navigator.pop(context),
        ),
        title: Text(
          appBarTitle,
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 10.0,
            ),
            Container(
              child: ListView.builder(
                shrinkWrap: true,
                itemCount: 3,
                itemBuilder: (context, index) {
                  return Container(
                    decoration: BoxDecoration(
                      color: AppColors.white,
                      borderRadius: BorderRadius.circular(15.0),
                      boxShadow: [
                        BoxShadow(
                          blurRadius: 10.0,
                          spreadRadius: 1.0,
                          color: AppColors.primaryColor.withOpacity(0.1),
                        ),
                      ],
                    ),
                    margin: const EdgeInsets.only(bottom: 20.0),
                    padding: const EdgeInsets.symmetric(
                      vertical: 14.0,
                      horizontal: 14.0,
                    ),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        CircleAvatar(
                          backgroundImage:
                              AssetImage('assets/images/profile.jpg'),
                          radius: 24.0,
                        ),
                        SizedBox(width: 20.0),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Anu priya Agrawal',
                              style: headingStyle,
                            ),
                            Row(
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text('Date of Joining', style: bodyStyle),
                                    Text('Expertise', style: bodyStyle),
                                    Text('Designation', style: bodyStyle),
                                    Text('Location', style: bodyStyle),
                                  ],
                                ),
                                Column(
                                  children: [
                                    Text(' : '),
                                    Text(' : '),
                                    Text(' : '),
                                    Text(' : '),
                                  ],
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text('1 March 2021', style: bodyStyle),
                                    Text('Risk', style: bodyStyle),
                                    Text('Consultant', style: bodyStyle),
                                    Text('Noida', style: bodyStyle),
                                  ],
                                )
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  );
                },
              ),
            ),
            Text(
              'Contact Us',
              style: Theme.of(context).textTheme.headline6.copyWith(
                    color: AppColors.primaryColor,
                    fontWeight: FontWeight.bold,
                  ),
            ),
            SizedBox(
              height: 10.0,
            ),
            Container(
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.black26,
                    blurRadius: 6.0,
                    spreadRadius: 2.0,
                  )
                ],
                color: AppColors.white,
              ),
              padding: const EdgeInsets.all(16.0),
              child: Column(
                children: [
                  Text(
                    'In case of any queries or information required, please feel free to contact us on',
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
                  Text(
                    'Peopleculture@in.gt.com',
                    style: Theme.of(context).textTheme.bodyText2.copyWith(
                          color: AppColors.primaryColor,
                          fontWeight: FontWeight.bold,
                        ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
