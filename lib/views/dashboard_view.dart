import 'package:flutter/material.dart';
import 'package:wcgtlake/utils/color_palette.dart';
import 'package:wcgtlake/views/drawer_view.dart';
import 'package:wcgtlake/views/home_view.dart';
import 'package:wcgtlake/views/knowledgebase_view.dart';

class DashboardView extends StatefulWidget {
  @override
  _DashboardViewState createState() => _DashboardViewState();
}

class _DashboardViewState extends State<DashboardView> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  int _currentIndex = 0;

  void onTabTapped(int index) {
    if (index == 1) {
      return;
    } else if (index == 2) {
      _scaffoldKey.currentState.openDrawer();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: DrawerView(),
      body: HomeView(),
      floatingActionButton: _buildFloatingActionBtn(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: _buildBottomAppBar(),
    );
  }

  Widget _buildBottomAppBar() {
    return BottomAppBar(
      child: BottomNavigationBar(
        currentIndex: _currentIndex,
        onTap: (value) {
          onTabTapped(value);
        },
        selectedItemColor: AppColors.primaryColor,
        unselectedItemColor: AppColors.dark_grey,
        backgroundColor: AppColors.white,
        items: [
          _buildBottomNavigationBarItem(
            'assets/images/home_grey.png',
            'assets/images/home_blue.png',
            'Home',
          ),
          _buildBottomNavigationBarItem(
            '',
            '',
            'Knowledge Base',
          ),
          _buildBottomNavigationBarItem(
            'assets/images/menu_grey.png',
            'assets/images/menu_blue.png',
            'More',
          ),
        ],
      ),
    );
  }

  BottomNavigationBarItem _buildBottomNavigationBarItem(
      String icon, String activeIcon, String label) {
    return BottomNavigationBarItem(
      icon: Padding(
        padding: const EdgeInsets.all(5.0),
        child: icon != ''
            ? Image.asset(
                icon,
                height: 24.0,
              )
            : SizedBox(
                height: 24.0,
              ),
      ),
      activeIcon: Padding(
        padding: const EdgeInsets.all(5.0),
        child: activeIcon != ''
            ? Image.asset(
                activeIcon,
                height: 24.0,
              )
            : SizedBox(
                height: 24.0,
              ),
      ),
      label: label,
    );
  }

  Widget _buildFloatingActionBtn() {
    return FloatingActionButton(
      onPressed: () => Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => KnowledgeBaseView(
            appBarTitle: 'Knowledge Base',
          ),
        ),
      ),
      backgroundColor: AppColors.accentColor,
      child: Image.asset(
        'assets/images/document.png',
        height: 32.0,
      ),
    );
  }
}

class BlankView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
