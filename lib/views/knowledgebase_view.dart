import 'package:flutter/material.dart';
import 'package:wcgtlake/utils/color_palette.dart';

class KnowledgeBaseView extends StatelessWidget {
  final String appBarTitle;

  const KnowledgeBaseView({
    Key key,
    this.appBarTitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_rounded,
          ),
          onPressed: () => Navigator.pop(context),
        ),
        title: Text(appBarTitle),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0),
                border: Border.all(color: AppColors.light_blue, width: 1.0),
              ),
              child: TextField(
                keyboardType: TextInputType.text,
                textInputAction: TextInputAction.search,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  prefixIcon: Icon(
                    Icons.search_rounded,
                    color: AppColors.light_blue,
                  ),
                  hintText: 'Search using keywords',
                  hintStyle: Theme.of(context)
                      .textTheme
                      .bodyText1
                      .apply(color: AppColors.light_grey),
                ),
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            Row(
              children: [
                Flexible(
                  flex: 2,
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                      border:
                          Border.all(color: AppColors.light_blue, width: 1.0),
                    ),
                    child: TextField(
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.search,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        prefixIcon: Icon(
                          Icons.search_rounded,
                          color: AppColors.light_blue,
                        ),
                        hintText: 'Search using filters',
                        hintStyle: Theme.of(context)
                            .textTheme
                            .bodyText1
                            .apply(color: AppColors.light_grey),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 10.0,
                ),
                Flexible(
                  flex: 1,
                  child: Container(
                    padding: const EdgeInsets.symmetric(vertical: 12.0),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                      border:
                          Border.all(color: AppColors.light_blue, width: 1.0),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          'Sort By',
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1
                              .apply(color: AppColors.light_grey),
                        ),
                        Icon(
                          Icons.keyboard_arrow_down_rounded,
                          color: AppColors.light_blue,
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            Expanded(
              child: ListView.builder(
                shrinkWrap: true,
                itemCount: 2,
                itemBuilder: (context, index) {
                  return Container(
                    decoration: BoxDecoration(
                      color: AppColors.white,
                      borderRadius: BorderRadius.circular(15.0),
                      boxShadow: [
                        BoxShadow(
                          blurRadius: 10.0,
                          spreadRadius: 1.0,
                          color: AppColors.primaryColor.withOpacity(0.1),
                        ),
                      ],
                    ),
                    margin: const EdgeInsets.only(bottom: 20.0),
                    padding: const EdgeInsets.symmetric(
                      vertical: 10.0,
                      horizontal: 10.0,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Text(
                          'Covid-19: Liquidity measures to consider for impacted',
                          style: Theme.of(context).textTheme.bodyText2.apply(
                              color: AppColors.primaryColor,
                              fontWeightDelta: 1),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Icon(
                                  Icons.calendar_today_rounded,
                                  color: AppColors.primaryColor,
                                ),
                                SizedBox(
                                  width: 10.0,
                                ),
                                Text(
                                  '1 March 2021',
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText1
                                      .apply(color: AppColors.primaryColor),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Icon(
                                  Icons.remove_red_eye_outlined,
                                  color: AppColors.primaryColor,
                                ),
                                SizedBox(
                                  width: 10.0,
                                ),
                                Text(
                                  '25',
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText1
                                      .apply(color: AppColors.primaryColor),
                                ),
                              ],
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Text(
                          'White paper on liquidity to measure consider for',
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1
                              .apply(color: AppColors.primaryColor),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Divider(
                          color: AppColors.light_grey,
                          height: 1.0,
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Text(
                          'Recovery and reorganization white paper',
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1
                              .apply(color: AppColors.dark_grey),
                        )
                      ],
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
