import 'package:flutter/material.dart';
import 'package:wcgtlake/utils/color_palette.dart';

class ToolsDatabaseView extends StatelessWidget {
  final String appBarTitle;

  const ToolsDatabaseView({
    Key key,
    this.appBarTitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_rounded,
          ),
          onPressed: () => Navigator.pop(context),
        ),
        title: Text(appBarTitle),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: Column(
          children: [
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10.0),
                border: Border.all(color: AppColors.light_blue, width: 1.0),
              ),
              child: TextField(
                keyboardType: TextInputType.text,
                textInputAction: TextInputAction.search,
                decoration: InputDecoration(
                  border: InputBorder.none,
                  prefixIcon: Icon(
                    Icons.search_rounded,
                    color: AppColors.light_blue,
                  ),
                  hintText: 'Search',
                  hintStyle: Theme.of(context)
                      .textTheme
                      .bodyText1
                      .apply(color: AppColors.light_grey),
                ),
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            Expanded(
              child: ListView.builder(
                shrinkWrap: true,
                itemCount: 2,
                itemBuilder: (context, index) {
                  return Container(
                    decoration: BoxDecoration(
                      color: AppColors.white,
                      borderRadius: BorderRadius.circular(15.0),
                      boxShadow: [
                        BoxShadow(
                          blurRadius: 10.0,
                          spreadRadius: 1.0,
                          color: AppColors.primaryColor.withOpacity(0.1),
                        ),
                      ],
                    ),
                    margin: const EdgeInsets.only(bottom: 20.0),
                    padding: const EdgeInsets.symmetric(
                      vertical: 10.0,
                      horizontal: 10.0,
                    ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Text(
                          'Ace Analyzer',
                          style: Theme.of(context).textTheme.bodyText2.apply(
                              color: AppColors.primaryColor,
                              fontWeightDelta: 1),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Text(
                          'Company database providing financial details of Indian listed and private companies',
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1
                              .apply(color: AppColors.primaryColor),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Divider(
                          height: 1.0,
                          color: AppColors.light_grey,
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                text: 'Account Owner : ',
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText1
                                    .apply(color: AppColors.dark_grey),
                              ),
                              TextSpan(
                                text: 'Meghna Bansal',
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText1
                                    .apply(color: AppColors.primaryColor),
                              )
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Divider(
                          color: AppColors.light_grey,
                          height: 1.0,
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                text: 'Team : ',
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText1
                                    .apply(color: AppColors.dark_grey),
                              ),
                              TextSpan(
                                text: 'Recovery & Reorganization',
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyText1
                                    .apply(color: AppColors.primaryColor),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
