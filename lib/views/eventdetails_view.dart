import 'package:flutter/material.dart';
import 'package:wcgtlake/utils/color_palette.dart';

class EventDetailsView extends StatelessWidget {
  final String appBarTitle;

  const EventDetailsView({
    Key key,
    this.appBarTitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_rounded,
          ),
          onPressed: () => Navigator.pop(context),
        ),
        title: Text(
          appBarTitle,
        ),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Sustainable Automotive Technologies - The road ahead for e-mobility',
              style: TextStyle(
                color: AppColors.primaryColor,
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(vertical: 20.0),
              child: Column(
                children: [
                  Row(
                    children: [
                      Icon(
                        Icons.person_outline,
                        color: AppColors.primaryColor,
                      ),
                      SizedBox(width: 5.0),
                      Text(
                        'Marketing Communications',
                        style: Theme.of(context).textTheme.headline6.copyWith(
                            color: AppColors.primaryColor,
                            fontWeight: FontWeight.w500),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Icon(
                        Icons.calendar_today_outlined,
                        color: AppColors.primaryColor,
                      ),
                      SizedBox(width: 5.0),
                      Text(
                        '28-Apr-2021 to 28-Apr-2021',
                        style: Theme.of(context).textTheme.headline6.copyWith(
                            color: AppColors.primaryColor,
                            fontWeight: FontWeight.w500),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Icon(
                        Icons.location_on_outlined,
                        color: AppColors.primaryColor,
                      ),
                      SizedBox(width: 5.0),
                      Text(
                        'Virutal Event',
                        style: Theme.of(context).textTheme.headline6.copyWith(
                            color: AppColors.primaryColor,
                            fontWeight: FontWeight.w500),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Icon(
                        Icons.access_time_outlined,
                        color: AppColors.primaryColor,
                      ),
                      SizedBox(width: 5.0),
                      Text(
                        '10:00 am to 5:00 pm',
                        style: Theme.of(context).textTheme.headline6.copyWith(
                            color: AppColors.primaryColor,
                            fontWeight: FontWeight.w500),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Text(
              'Event Description',
            ),
            SizedBox(
              height: 20.0,
            ),
            Container(
              alignment: Alignment.center,
              child: Column(
                children: [
                  Text('To register for the event, please write to '),
                  Text(
                    'GTBharat@in.gt.com',
                    style: TextStyle(color: AppColors.primaryColor),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
