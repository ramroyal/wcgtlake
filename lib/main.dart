import 'package:flutter/material.dart';
import 'package:wcgtlake/utils/color_palette.dart';
import 'package:wcgtlake/views/dashboard_view.dart';

void main() {
  runApp(WcgtLake());
}

class WcgtLake extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'WcgtLake Application',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: AppColors.primaryColor,
        accentColor: AppColors.accentColor,
        scaffoldBackgroundColor: AppColors.white,
        fontFamily: 'GTWalsheimPro',
        textTheme: TextTheme(
          headline5: TextStyle(
            color: AppColors.primaryColor,
            fontSize: 20.0,
            fontWeight: FontWeight.w800,
            letterSpacing: 0.5,
          ),
          headline6: TextStyle(
            fontSize: 18.0,
            fontWeight: FontWeight.w600,
          ),
          bodyText2: TextStyle(
            fontSize: 16.0,
            fontWeight: FontWeight.w600,
          ),
          bodyText1: TextStyle(
            fontSize: 14.0,
            fontWeight: FontWeight.w600,
          ),
        ),
        iconTheme: IconThemeData(color: AppColors.primaryColor),
        appBarTheme: AppBarTheme(
          backgroundColor: AppColors.white,
          elevation: 0.0,
          textTheme: TextTheme(
            headline6: TextStyle(
              fontFamily: 'GTWalsheimPro',
              color: AppColors.primaryColor,
              fontSize: 18.0,
              fontWeight: FontWeight.bold,
            ),
          ),
          iconTheme: IconThemeData(color: AppColors.primaryColor),
        ),
      ),
      home: DashboardView(),
    );
  }
}

// BottomNavigationBar(
//         currentIndex: _currentIndex,
//         onTap: (value) {
//           onTabTapped(value);
//         },
//         selectedItemColor: AppColors.primaryColor,
//         unselectedItemColor: AppColors.dark_grey,
//         backgroundColor: AppColors.white,
//         items: [
//           _buildBottomNavigationBarItem(
//             'assets/images/home_grey.png',
//             'assets/images/home_blue.png',
//             'Home',
//           ),
//           _buildBottomNavigationBarItem(
//             '',
//             '',
//             'Knowledge Base',
//           ),
//           _buildBottomNavigationBarItem(
//             'assets/images/menu_grey.png',
//             'assets/images/menu_blue.png',
//             'More',
//           ),
//         ],
//       ),
